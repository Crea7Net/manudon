# [Retour Accueil](README.md)
## Usage de Homestead dans Vagrant avec VSCode

### Installer
- [Visual Studo Code](https://c57.fr/doc/outils/vscode)
- [VirtualBox](https://c57.fr/doc/outils/gitkraken)
- [Vagrant](https://www.vagrantup.com/docs/installation)
- [Git](https://git-scm.com/)
- Homestead:
```ruby
vagrant box add laravel/homestead

git clone https://github.com/laravel/homestead.git ~/Homestead

cd Homestead

bash init.sh

vagrant init
```

Réglages de:
/Homestead/Homestead.yaml
(Dont clé SSH)

```ruby
vagrant up
vagrant up --provision
vagrant suspend
vagrant destroy
vagrant ssh
```

## [Une question ?](https://gitlab.com/grcote7/manudon/issues)

## Laravel


laravel new questions

Le .env (Doit aussi avoir les params de la cnx DB)
config/app.php

app/http/Controllers/auth: $redirectTo ='/compte'

### Les emails
MailHog
http://localhost:8025

### Les DB

DB: MySQL Workbench ou Heidi (Préférence)

artisan make:migration create_mags_table --create=mags

artisan migrate --seed

php artisan config:clear

artisan make:migration add_to_mags_table --table=mags

N.B. Pour nouveau seeding: composer dump-autoload
artisan migrate:fresh --seed

Seeder avec Faker

artisan make:factory QuestionFactory --model=Question

php artisan make:seeder QuestionsTableSeeder

### Les modèles

```ruby
artisan make:model Post -m 
```

Même nom que table au singulier et majuscule
L'option -m créée les fichiers de migration
protected $table dans model si nom de table différent du model

### Les relations

One to one (hasOne) & inverses (BelongsTo)

    One to Many (hasMany) & inverses (BelongsTo)


### Les routes

routes/**web.php**

GET/POST/PUT/DELETE

```ruby
artisan route:list
```

100 X + rapide

```
artisan route:cache
```

Suppr du cache

```
artisan route:clear
```

Exemple de route avec closure:

    Route::get('answer/{answer?}', function (App\Answer $answer) {
        // return $answer['answer'];
        // ou
        return $answer->answer;
    })->name('answer');


Toutes les routes si controller créé avec:

  artisan make:controller AskController --resource --model=Question

```ruby
Route::resource('posts', 'PostController');
```

```ruby
artisan make:auth
```

Désactiver le cache

artisan route:clear

(Ne fonctionne pas avec les routes contenant une closure)

### Les Controllers

```
artisan make:controller QuestionController

```

```
artisan make:controller QuestionController --resource

```

#### TOP:
```
    artisan make:controller AskController --resource --model=Question
```

=> Créée aussi l'injection des models (Injection de dépendances)

Pour n'avoir que les routes voulues

```
Route::resource('questions', 
    'QuestionController')
    ->only('show');

Route::resource('questions',
    'QuestionController')
    ->only(['show', 'update']);

Route::resource('questions',
    'QuestionController')
    ->except(['show', 'update']);

```

```
Route::resource('questions', 
  'QuestionController');
```

(NB: Ne pas la nommer)

Ds app\Providers\AppServiceProvider.php

    public function boot()
    {
        Route::resourceVerbs([
            'create' => 'creer',
            'edit'  => 'editer',
        ]);
    }

### Les Middleware

Dans le constructeur:
Exemple:

```
$this->middleware('auth')->except('index', 'show');
```

Il existe aussi ->only()



### Les Vues & Blade

Layout

    {{ asset('*pathFile*') }}
```
@yield('content')
```

Vu install dépendance Gravatar:
```
<img src="{{ Gravatar::get(Auth::user()->email) }}" alt="">
```

Dans View

    @extends('layouts.app')
    @section('content')
    ...
    @endsection
    
    {{ route('nomroute') }}

    Dans Form
    @csrf
    Pour hiddem {{  $token }}
    
    @method('PUT')
    (Exemple pour route compte.update)



### Policies

```ruby
artisan make:policy QuestionPolicy
  --model=Question
```


## Outils :

### npm / webpack

```ruby
npm i -D webpack --no-bin-links
npm i -D webpack-cli --no-bin-links
sudo npm install --global webpack
sudo npm install --global webpack-cli
sudo npm install --global cross-env

[Pour bootstrap]
npm install -g node-sass --unsafe-perm --no-bin-links

```

sudo npm i -g webpack

sudo npm i -g webpack-dev-server

(--no-bin-links evite erreur de symlink dans VM)

### Déploiement

- [ ] 2do Forge
Forge (Créé par fondateur)
https://forge.laravel.com/auth/register

Bitbucket



Mailgun
https://app.mailgun.com/app/dashboard


GL - Intégration continue
Juste entrer les param ftp

NB: Pour redir ds public (POur réel uniquement)
Renommer server.php en index.php + .htaccess en root

### Sav de toutes les BdD de Homestead

```
mysqldump -u homestead -psecret --all-databases > homestead-backup.sql
```

**Récupération en local**

Voir lke dossier local des fichiers, racine du site ou commande executée


--------------------------------------

Droits des dossiers OCMS

2do à mettre ds c57

https://laravel-news.com/install-octobercms-ubuntu

Permissions
Our job is not done and we must set the correct permissions on a few directories. In the site root run:

sudo chmod -R 775 storage/
sudo chmod -R 775 themes
