<?php

/*
 * (c) Manudon - 2019
 */

 // Une fonction anonyme pour un affichage très lisible d'une variable, même si array
$vd = function ($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
};

$x = [1, 2, 3];
var_dump($x); // Sera lisible que en local si xdebug pré-installé dans le navigateur, ou par le server local

$vd($x); // Simple, rapide à taper, sera toujours bien lisible

?>
<a href="http://prntscr.com/owla8y" target="_blank">Voir différence entre simple var_dump et $vd()</a>