<?php

/*
 * (c) Manudon - 2019
 */

echo '__FILE__ : '.__FILE__.'<hr />';
echo '__DIR__ : '.__DIR__.'<hr />';
echo '__LINE__ : '.__LINE__.'<hr />';
define(RACINE, __DIR__);
echo 'RACINE : '.RACINE.'<hr />';
define(FILENAME, basename(__FILE__));
echo 'FILENAME : '.FILENAME.'<hr />';
/*
echo '__FUNCTION__ : '.__FUNCTION__.'<hr />' ;
echo '__CLASS__ : '.__CLASS__.'<hr />' ;
echo '__METHOD__ : '.__METHOD__.'<hr />' ;
echo '__NAMESPACE__ : '.__NAMESPACE__.'<hr />' ;
echo '__TRAIT__ : '.__TRAIT__.'<hr />' ;
*/