<?php

/*
 * (c) Manudon - 2019
 */

$aleatoire = rand(0, 100);
$fonction  = function () {
    echo 'Bleurg 001'.'<hr>'; // NB: On met les retour à la ligne en fin d'affichage
};

$fonction(); // Appelle la fonction anonyme ci-dessus grâce à la variable dans lmaquelle elle a été préalsablement stockée

echo 'N aléatoire entre 0 & 100: <b>'.$aleatoire.'</b><hr>';

function Bonjour($nom = null) // Permets d'appeler la fonction avec ou sans argument
{
    // Elle ne peut être définie qu'une seule fois, car elle a pour rôle d'être constante !
    // Donc, impératif ici, puisque dans une fonction qui 'risque' d'être appelée plusieurs fois, car sinon, peu d'intérêt d'en avoir fait une fonction...
    if (!defined('BONJOUR_NOM')) {
        define('BONJOUR_NOM', $nom);
    }

    static $xyz;
    global $abc;

    $nom = 'Histoire d\'avoir qqch dedans';
    $xyz = $abc = $nom;

    echo '(fonction Bonjour) Bonjour '.BONJOUR_NOM.' :-) !!!'.'<hr>';
    echo '(fonction Bonjour) $xyz '.$xyz.' :-) !!!'.'<hr>';

    return 'BleurgGGGGHHHH !';
    echo '(fonction Bonjour) Bonjour JAMAIS EXECUTEE CAR APRES UN RETURN '.BONJOUR_NOM.' :-) !!!'.'<hr>';
}
Bonjour('Jacks');
Bonjour('Fabrice');

echo '<h1>'.Bonjour().'</h1>'; // Utilise ce que renvoie la fonction sans argument

echo BONJOUR_NOM.'<hr>';

echo 'static $xyz : '.$xyz.'<hr>'; // La portée d'une variable static (Comme toutes variable en générale, sauf global) est local donc invisible ici => Rien ne s'affiche
// (En local avec un paramètre élévé de reporting_error, probablement au moins une notice...)

echo 'global $abc : '.$abc.'<hr>'; // A contrario, même si instancée dans une fonction, la portée d'une variable de type global est... Globale ! Donc visible ici, même en dehors de la fonction