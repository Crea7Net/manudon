<?php

/*
 * (c) Manudon - 2019
 */

// Simple IF

$vrai = true;

if ($vrai) {
    echo '$vrai est '.$vrai; // On voit que true est transformé en un 1
}
var_dump($vrai); // Mais on voit là que le type est bien boolean, et qd la valeur est 1, ça affiche ici bel et bien true, et sinon, false
echo '<hr>';

// IF / ELSE

$animal = 'mammifère';
if ('reptile' === $animal) {
    $type = 'reptile';
} else {
    $type = 'mammifère';
}
echo 'L\'animal est un '.$type.'<hr>';

// Autre exemple
$age     = 18;
$inscrit = false;
if ($age >= 18) {
    if ('abonné' === $inscrit) {
        echo 'Bienvenue :)';
    } elseif ('demo' === $inscrit) {
        echo 'Bienvenue sur la page limitée ';
    } else {
        echo 'Souhaitez-vous vous inscrire ? ';
    }
} else {
    echo 'Dégage petit :-( !'; // Pas de de pitié !!! Lol
}
echo '<hr>';

// Ternaires.
$majeur  = $animal  = $age  = $inscrit  = 0;
$age     = 17;
$majeur  = ($age > 17 ? true : false); // Si majeur vrais /faux
$inscrit = ($majeur ? 'Ok' : 'Pas good'); // Retranscription en texte

echo 'Si on a '.$age.' ans, alors l\'autorisation d\'entrée est : '.$inscrit.' !<hr>';

// switch
$animal = 'hiboux';

switch ($animal) {
    case 'chien':
        echo 'C\'est un chien';

        break;
    case 'girafe':
        echo 'C\'est une girafe';

        break;
    case 'rat':
        echo 'C\'est un rat';

        break;
    case 'éléphant':
        echo 'C\'est un éléphant';

        break;
    case 'hiboux':
        echo 'C\'est un hiboux';

        break;
    case 'pivoine':
        echo 'C\'est un pivoine';

        break;
    case 'chat':
        echo 'C\'est un chat';

        break;
    default:
        echo 'Pffff... Aucune idée ! ';
}
// echo '<hr>';