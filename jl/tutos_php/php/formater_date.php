<?php

/*
 * (c) Manudon - 2019
 */

date_default_timezone_set('Europe/Paris'); // Pour être dans notre crénaux horaire

$date = date('d/m/y');
// echo 'Heure du serveur: Nous sommes le '.$date.' et il est '.date('H:i:s').'<hr>';

$fmt = new IntlDateFormatter(
    'fr-FR',
    null,
    null,
    'Europe/Paris',
    IntlDateFormatter::GREGORIAN
);

// echo 'Le modèle du formateur est : '.datefmt_get_pattern($fmt);
datefmt_set_pattern($fmt, 'EEEE dd MMMM y à HH:mm:ss');
echo 'Date en toutes lettres: '.ucwords($fmt->format(time())).'<hr>';