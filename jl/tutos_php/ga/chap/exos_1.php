<?php

/*
 * (c) Manudon - 2019
 */

$nombre_secret = rand(0, 10);
$nbUtilisateur = null;
$nbCoups       = 0;
echo $nombre_secret.'<hr>';
while ($nombre_secret !== $nbUtilisateur) {
    ++$nbCoups; // Une varablie qui compte les coups
    echo '<h1>'.$nbCoups.'</h1>';
    $nbUtilisateur = (int) readline(' Le nombre mysterieux est : ');
    if ($nombre_secret < $nbUtilisateur) {
        echo 'Trop grand  '."\n";
    } elseif ($nombre_secret > $nbUtilisateur) {
        echo 'Trop petit '."\n";
    } elseif ($nombre_secret === $nbUtilisateur) {
        echo 'Bravo vous avez trouver le nombre mysterieux ! '.$nombre_secret.'  ';

        break;
    } else {
        echo 'Donnée inconpréhensible ';
    }
    if ($nbCoups > 999) { // Un test qui si 1000 essais ont été faits, et n'ont pas permis de trouver le nombre secret, arrête la boucle de froce
        // Une excellente précaution, car ici:
        // http://jl/ga/chap/exos_1.php
        // Le script tournait sans jamais s'arrêter !
        // Pas grave en local, mais une boucle infinie comme celle-ci t'écroule le vrai serveur en moins de temps qu'il ne faut pour le dire !!! ;-)
        // (En effet, les navigateurs ne comprennent pas readline()...
        // D'où l'intérêt de faire la même chose, mais avec un bête formulaire comme ici: gc7\tutos\jeu.php
        break;
    }
}

$nbUtilisateur = (int) readline('le nombre mysterieux est :'."\n");