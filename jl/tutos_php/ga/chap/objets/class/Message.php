<?php

use Faker\Provider\ka_GE\DateTime;

use function GuzzleHttp\Psr7\str;

class Message
{
    private $userName ;
    private $message ; 
    private $tableau ;
    private $date ;

    public function __construct(string $userName , string $message , DateTime $date=null)
    {
        $tableau = [$userName , $message ] ;
        $this -> userName =  $userName ;
        $this -> message =   $message ;
        
        
        if ( $this ->isValid( $this -> userName , $this -> message )  ) 
        {
            $this ->fromJson( $this -> userName , $this -> message ) ;
        }

    }

    private function isValid( $userName , $message ):bool
    {
        $nb = strlen( $this -> userName ) ;
        
        if( $nb <= 6 && $nb >= 255 )
        {
            return false ;
        }
        elseif ( is_int( $userName ||  is_int( $message ) ) )
        {
            return false ;
        }

        return true ;
    }

    private function fromJson( $userName , $message ):void
    {
        $tableau = [ $userName , $message] ;
        $this -> tableau =  json_encode( $tableau ) ;
    }
    public function ajout()
    {
        file_put_contents('livre d\'or.txt', $this->tableau."\n" , FILE_APPEND ) ;
    }

    
}

?>