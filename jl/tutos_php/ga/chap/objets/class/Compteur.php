<?php
class Compteur
{
    public $chemin ;
    private $fichier ;
    private $nb ;

    public function __construct( $chemin )
    {
        $this->chemin = $chemin ;
        
        $this->ouvertureFichier() ;
        $this->fermetureFichier() ;
    }

    public function lectureFichier():int
    {
        $lecture = (int)file_get_contents( $this->chemin ) ;
        return $lecture ;
    }

    public function incrementeFichier():void
    {
        $this->nb = (int)file_get_contents( $this->chemin ) ;
        $this->nb++;
        $this->nb = (int)file_put_contents( $this->chemin , $this->nb ) ;
    }

    private function ouvertureFichier()
    {
        $this->fichier = fopen( $this->chemin , 'c+') ;
    }

    private function fermetureFichier()
    {
        fclose( $this->fichier ) ;
    }



}
?>