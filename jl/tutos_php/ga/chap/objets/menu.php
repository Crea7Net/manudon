<?php
$racine = __DIR__ ;

if( $_SERVER['SERVER_NAME'] != 'manudon.com' )
{
  define( 'RACINE' , 'http://'.$_SERVER['SERVER_NAME'].'/tutos_php/ga/chap/objets' ) ;
}
else {
  define( 'RACINE' , 'http://'.$_SERVER['SERVER_NAME'].'/jl/tutos_php/ga/chap/objets' ) ;;
}

$racine = RACINE ;
function ajoutLien(string $page = '', string $lien='A completter',string $class='nav-link' , $racine = RACINE ): string {
//  var_dump( RACINE ) ; //EFFACE-MOI

  return <<< monLien
  <li class="nav-item active">
    <a class="nav-link" href="$racine/$page">$lien <span class="sr-only">(current)</span></a>
  </li>
monLien;

}
?>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Fixed navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

      <ul class="navbar-nav mr-auto">

        <?= ajoutLien('index.php', 'Acceuil' ) ; ?>
        <?= ajoutLien('TP/compteur.php', 'compteur' ) ; ?>
        <?= ajoutLien('TP/Livre_d_or.php', 'Livre d\'or' ) ; ?>

      </ul>

      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>