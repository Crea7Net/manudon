<?php

/*
 * (c) Manudon - 2019
 */

ini_set('display_errors', 1);
$path = ('manudon.com' === $_SERVER['HTTP_HOST']) ? '/gc7' : '../../gc7';
include_once '../../../../gc7/tools/vd.php';
echo '<link rel="stylesheet" href="../../../assets/css/style.css">';

// echo "../../../../${path}/assets/css/style.css";
$nom    = 'doe';
$prenom = 'marc';
$notes  = [
    'note n°1' => 9,
    'note n°2' => 19,
    'note n°3' => 18,
    'note n°4' => 19,
];
$moyenne = round(array_sum($notes) / count($notes), 2);
$max1    = max($notes);
// $t1              = [1, 2, 3];
// $t2              = [1, 2, 3];
// svd('t1', 't2');

echo 'Bonjour, '.ucfirst($prenom).' '.strtoupper($nom).'.<br><br>                   Vous avez eu '.$moyenne.' de moyenne avec une note maximale de '.$max1.', la première fois avec la '.array_keys($notes, $max1, true)[0].'.'."\n<br><br>";

vd($notes, array_keys($notes, max($notes), true)); // On peut poser direct des fonctions dedans !

//include 'uuu.php';

$lio = 'Oki';
$a   = [1, 2, 'c' => 'd'];

echo '<h2>Fais ton choix ;-) !</h2>';

vd($a, $lio); // On utilise les vrais noms des variables - Souvent la plus simple
svd('a', 'lio'); // On utilise les noms des variables sans le $, et chacun dans une chaîne de car. Bilan: On les retrouve aussi dans le tablo du debug :-)
pr($a, $lio); // On utilise les vrais noms des variables - Souvent la plus simple
spr('a', 'lio'); // Idem, mais ,+ clair mais ignore les éléments vides ou nuls.