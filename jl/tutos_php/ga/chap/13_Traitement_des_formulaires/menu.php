<?php
$racine = __DIR__ ;

if( $_SERVER['SERVER_NAME'] != 'manudon.com' )
{
  define( 'RACINE' , 'http://'.$_SERVER['SERVER_NAME'].'/tutos_php/ga/chap' ) ;
}
else {
  define( 'RACINE' , 'http://'.$_SERVER['SERVER_NAME'].'/jl/tutos_php/ga/chap' ) ;;
}

function ajoutLien(string $page = '', string $lien='A completter',string $class='nav-link' ): string {

  return <<< monLien
  <li class="nav-item active">
    <a class="nav-link" href="$page">$lien <span class="sr-only">(current)</span></a>
  </li>
monLien;

}
?>
<!-- Fixed navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="#">Fixed navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">

    <ul class="navbar-nav mr-auto">

        <?= ajoutLien(RACINE.'/13_Traitement_des_formulaires/index.php', 'Acceuil' ) ; ?>
        <?= ajoutLien(RACINE.'/13_Traitement_des_formulaires/jeu.php', 'jeux' ) ; ?>
        <?= ajoutLien(RACINE.'/13_Traitement_des_formulaires/glace.php', 'Glace' ) ; ?>
        <?= ajoutLien(RACINE.'/14_dates.php', 'Contact' ) ; ?>
        <?= ajoutLien( RACINE.'/15_Lecture_fichiers.php', '15, Lecture de fichiers' ) ; ?>

    </ul>

    <form class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>