<canvas id="particle-canvas"></canvas>
<script src="../particle.js"></script><?php
//    require_once '/home/ego/code/manudon/gc7/tools/vd.php' ;

/**
 * Retourne le code HTML pour l'affichage dans une liste groupée selon bootstrap.
 *
 * On appelle ce type de fonction un helper
 *
 * @param string $sujet
 * @param string $lien
 */
function lk($sujet, $lien , $racine=null )
{
  if( $_SERVER['SERVER_NAME'] != 'manudon.com' )
  {
    $racine = null ?? 'http://'.$_SERVER['SERVER_NAME'].'/tutos_php/ga/chap/' ;
  }
  else {
    $racine = null ?? 'http://'.$_SERVER['SERVER_NAME'].'/jl/tutos_php/ga/chap/' ;
  }
    return '<li class="list-group-item p-1 pl-2"><a href="'.$racine.$lien.'">'.$sujet.'</a>
    </li>';
}
?>

<hr>
<a href="a/uuu.php">Test sur manudon.com</a>
<hr>

<ul class="list-group my-3">
  <?php echo lk('01 Bases du php', '01_bases_du_php.php'); ?>
  <?php echo lk('05 Les Variables', '05_variable.php'); ?>
  <?php echo lk('06 Les tableaux', '06_tableaux.php '); ?>
  <?php echo lk('08  les boucles', '08_les_boucles.php '); ?>
  <?php echo lk('09 Les fonctions', '09_fonctions.php '); ?>
  <?php echo lk('11 Require Include', '11_Require_Include.php'); ?>
  <?php echo lk('12 PHP & HTML', '12_PHP_&_HTML'); ?>
</ul>
<ul class="list-group my-3">
    <?php echo lk(' 11 Require Include  ', '11_Require_Include.php'); ?>
    <?php echo lk(' 12 PHP & HTML ', '12_PHP_&_HTML'); ?>
    <?php echo lk(' 13 Traitement des formulaires ', '13_Traitement_des_formulaires'); ?>
    <?php echo lk('14 Les dates ', '14_dates.php'); ?>
    <?php echo lk('15 Lecture de fichiers', '15_Lecture_fichiers.php'); ?>
    <?php echo lk('23, l\'objet datetime', '23_objet_DateTime.php'); ?>
    <?php echo lk('TP/Livre_d_or.php', 'Livre d\'or' ) ; ?>
    <?php echo lk('______', '_______.php'); ?>
    <?php echo lk('______', '_______.php'); ?>
</ul>
<h2>Objets</h2>
<ul class="list-group my-3">
    <?php echo lk('compteur', 'objets/TP/compteur.php'); ?>
    <?php echo lk('Livre d\'or', 'objets/TP/Livre_d_or.php'); ?>
    <?php echo lk('______', '_______.php'); ?>
</ul>