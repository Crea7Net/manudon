<?php

/*

Hey, Jean-Luc...


Vu ce que je vois dans ce fichier et toutes, les parties qui se mettent en place, je crois qu'il serait bon que tu regardes

https://www.grafikart.fr/tutoriels/html-template-php-1122

(Et le suivant, qui traite justement et en particulier des formulaires, une histoire de glaces... Miam ! Lol - Note, je l'ai suivi hier, et du coup, c'est ce que tu vois ds mon dossier GC7 de MnD...
En fait, comme me l'avait dit Momo, il s'agît d'une série... D'ailleurs son dossier sqlite, c'est une des exercices de la fin de cette série

Je t'invite à regarder ces qqes vidéos, car GA (GrafikArt) est une référence :-)
Moi, j'ai commencé du début

https://www.grafikart.fr/tutoriels/php-presentation-1112

et j'en suis déjà au 15ème en qqes heures, sachant que je ne fais pas que cela... C'est juste que ce tuto est très récent, et donc, les infos dedans optimales :-)

Bon, sinon, n'hésite pas à bien voir les modifs effectuées avec ton GK depuis une branche que j'ai appelée "PourJL-Verif"

Vérifie depuis là, chaque dossier de chaque commit, car je revois dans ce code des petites erreurs déjà signalées

(Ex. <hr> et non <hr /> (ça, ct il y a 10 ans ! ;-)...  )

Mais surtout, aussi, avant de te lancer ds un tel script, assure tes bases, car il est vital de découper les services de ton scripts, sinon, ça va vite devenir une usine à gaz...

Allez, courage, et...


...@++ :-)


*/




ini_set('display_errors', 1);
			
include_once('../fonction/racine.php');
include_once("$racine/Vue/head.html") ;

include_once("$racine/Vue/menu.html") ;

/**
 * Création de la Table 
 * ( En Construction )
 */
//	include_once("$racine/fonction/creationTable.php");


/**
 * Création de la fonction retour a l'index Flash-Cards
 */
$action= $_GET['Action'];
$os= $_GET['OS'];

if( !$action ) //Arriver sur la page 
{
	/**
	 * Un p'tit message 
	 * +
	 * le formulaire
	 */
?>
<h1>Installation du site Flash - Cards</h1>
<article>
    <h2>Bienvenue sur la page d'installation</h2>
    <p>Pour installer le site cliquer simplement sur le bouton création du site</p>
    <p>Une base de donnée sera créée puis une fonction extrêmement importante sera placée à la racine du site.</p>
    <p>En effet ce site fonctionne grace au
        <pre>\$_SESSION[]</pre>
    </p>
    <p>Une vérification permanente s'impose</p>
    <p>D'avance, je vous remercie pour les retours de vos expériences</p>
</article>
<hr>

<form methode='GET'>
    <input type='submit' name="Actions" value="Création du site">

    <hr />
    <div>
        <input type="radio" id="Linux" name="OS" value="Linux" checked>
        <label for="Linux">Un serveur Linux</label>
    </div>

    <div>
        <input type="radio" id="Windows" name="OS" value="Windows">
        <label for="Windows">Un serveur Windows ( Pas encore opérationnel )</label>
    </div>

</form>
<hr>
<?php
}
elseif( $action == 'Création du site' )
{
	/**
	 * Détection automatique du nom du serveur
	 * Détection automatique du chemin vers la racine du site
	 * Détection automatique du chemin vers l'index depuis la racine du site
	 */
	$erveur_name = $_SERVER['SERVER_NAME'] ;// EFFACE - MOI
	global $uuu ;// EFFACE - MOI
	$uuu = $_SERVER['PHP_SELF'] ;// EFFACE - MOI
	$uuu = $_SERVER['SERVER_NAME'] ;// EFFACE - MOI
	$uuu = $_SERVER['DOCUMENT_ROOT'] ;// EFFACE - MOI


	$chemin='http://'. $_SERVER['SERVER_NAME'] .$_SERVER['PHP_SELF'].'/';
	/**
	 * Elimination du chemin inutile vers l'intallateur
	 */
	$chemin= str_replace('/controleur/install.php','',$chemin) ;

	/**
	 * Ouverture + création du fichier contenant la fonction de retour a l'index ...
	 * 
	 * ... Puis on place la fonction a l'intérieur
	 * 
	 */

	$retourIndexFlashCards = fopen('fonctionRetourIndexFlashCard.php','w+') ;
	$ecriture = <<<ECRITURE
<?php
header('Location:$chemin');
exit() ;
/**
* Petit script a placer au début de CHAQUES pages
* si les sessions sont actives => réactualisation 
* si les sessions sont inactives => retour a l'index 
*/
?>
\$r=( \$_SESSION['racine'] ? session_start() : header('Location:
http://$erveur_name/fonctionRetourIndexFlashCard.php'));
ECRITURE;


if( $os == 'Linux' )
{
/**
* Déplacement de la fonction php ver la racine du site
* Version Linux
*/

$output = shell_exec('mv fonctionRetourIndexFlashCard.php '. DOCUMENT );
echo "
<pre><h1>$output</h1></pre>";
}
elseif( $os == 'Windows' )
{
echo 'Windows';
/**
* Déplacement de la fonction php ver la racine du site
* Version Windows
*
* Pas encors opérationnel
*
*/
}
else{
define(ERREUR, '<h1>UNE ERREUR INATTENDUE EST SURVENUE !</h1>
<p>Veuillez recommencer l\'installation merci</p>');
echo ERREUR ;
}
echo "
<pre>http:// + SERVER_NAME + PHP_SELF  $chemin</pre>";

fwrite($retourIndexFlashCards , $ecriture );

fclose($retourIndexFlashCards);
}
else{
define(ERREUR, '<h1>UNE ERREUR INATTENDUE EST SURVENUE !</h1>
<p>Veuillez recommencer l\'installation merci </p>');
echo ERREUR;
}
?>
<hr>
<h1>DOCUMENT => <?=DOCUMENT?></h1>
<hr><!-- EFFACE - MOI-->

<?php
include_once("$racine/Vue/footer.html");