@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-home"></i> @lang('navs.general.home')
            </div>
            <div class="card-body">
                @lang('strings.frontend.welcome_to', ['place' => app_name()]) (<a
                    href="https://gitlab.com/grcote7/manudon" class="text-decoration-none" title="Dépôt GitLab"
                    target="_blank">GL <i aria-hidden="true" class="fa fa-external-link"></i></a>)
            </div>
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
<!--row-->

<div class="row mb-4">
    <div class="col">
        @include('modules.blocks.linkssites')
    </div>
    <!--col-->
</div>
<!--row-->

<div class="row mb-4">
    <div class="col">
        @include('modules.thumbnails.userssites')
    </div>
    <!--col-->
</div>
<!--row-->

<!--
    <div class="row mb-4">
        <div class="col">
            <example-component></example-component>
        </div>
    </div>
    -->

<div class="row mb-5">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <i class="fab fa-font-awesome-flag"></i> Font Awesome @lang('strings.frontend.test')
            </div>
            <div class="card-body">
                <i class="fas fa-home"></i>
                <i class="fab fa-facebook"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-pinterest"></i>
            </div>
            <!--card-body-->
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
<!--row-->
@endsection