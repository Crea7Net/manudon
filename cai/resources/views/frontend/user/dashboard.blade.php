@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <strong>
                    <i class="fas fa-tachometer-alt"></i> @lang('navs.frontend.dashboard')
                </strong>
            </div>
            <!--card-header-->

            <div class="card-body">
                <div class="row">
                    <div class="col col order-1 order-sm-2 mb-4">
                        <div class="card mb-4 bg-light">
                            <img class="card-img-top mx-auto" src="{{ $logged_in_user->picture }}" alt="Profile Picture">

                            <div class="card-body">
                                <h4 class="card-title">
                                    {{ $logged_in_user->name }}<br />
                                </h4>

                                <p class="card-text">
                                    <small>
                                        <i class="fas fa-envelope"></i> {{ $logged_in_user->email }}<br />
                                        <i class="fas fa-calendar-check"></i> @lang('strings.frontend.general.joined')
                                        {{ timezone()->convertToLocal($logged_in_user->created_at, 'F jS, Y') }}
                                    </small>
                                </p>

                                <p class="card-text">

                                    <a href="{{ route('frontend.user.account')}}" class="btn btn-info btn-sm mb-1">
                                        <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.account')
                                    </a>

                                    @can('view backend')
                                    &nbsp;<a href="{{ route('admin.dashboard')}}" class="btn btn-danger btn-sm mb-1">
                                        <i class="fas fa-user-secret"></i> @lang('navs.frontend.user.administration')
                                    </a>
                                    @endcan
                                </p>
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">Recommendation</div>
                            <div class="card-body">
                                <h4 class="card-title">Soyez régulier</h4>
                                <p class="card-text">Vous connaissez le Lièvre et la Tortue...?<br>
                                    Bien...: Un codeur averti en vaut 2 <i class="fa fa-smile-o" aria-hidden="true"></i> !
                                </p>
                            </div>
                        </div>
                        <!--card-->
                    </div>
                    <!--col-md-4-->

                    <div class="col-md-8 order-2 order-sm-1">
                        <div class="row">
                            <div class="col">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <h5>Dernières infos</h5>
                                    </div>
                                    <!--card-header-->

                                    <div class="card-body">

                                        <p>27/08/2019: Début du dev du concept ManuDon</p>
                                    </div>
                                    <!--card-body-->
                                </div>
                                <!--card-->
                            </div>
                            <!--col-md-6-->
                        </div>
                        <!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        Box 1
                                    </div>
                                    <!--card-header-->

                                    <div class="card-body">
                                        Tatati 1.
                                    </div>
                                    <!--card-body-->
                                </div>
                                <!--card-->
                            </div>
                            <!--col-md-6-->

                            <div class="col">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        Box 2
                                    </div>
                                    <!--card-header-->

                                    <div class="card-body">
                                        Tatati 2.
                                    </div>
                                    <!--card-body-->
                                </div>
                                <!--card-->
                            </div>
                            <!--col-md-6-->

                            <div class="w-100"></div>




                        </div>
                        <!--row-->
                    </div>
                    <!--col-md-8-->
                </div><!-- row -->
            </div> <!-- card-body -->
        </div><!-- card -->
    </div><!-- row -->
</div><!-- row -->
@endsection
