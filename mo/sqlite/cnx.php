<?php

/*
 * (c) Manudon - 2019
 */

$server = $_SERVER['SERVER_SOFTWARE'];
// var_dump($server, stripos($server, 'nginx'));

$cnxs = [
    'sg' => [ // Le vrai serveur: http://manudon.com/mo/sqlite/
    // Nom du HOST: sg.NewNRJ.com
        'db' => 'admin_mans',
        'u'  => 'admin_jlmoromgc7',
        'pw' => 'secret',
    ],
    'li' => [ // Conf ngninx de Li
        'db' => 'admin_mans',
        'u'  => 'homestead',
        'pw' => 'secret',
    ],
    'mo' => [ // Conf ngninx de Mo
        'db' => 'admin_mans',
        'u'  => 'homestead',
        'pw' => 'secret',
    ],
    'moXampp' => [ // Con Apache local de Mo avec xampp
        'db' => 'products',
        'u'  => 'root',
        'pw' => '',
    ],
];

// die('ok 20');
// Li utilise un server nginx:
// Cette ligne identifie le type de server et affecte les params ad'hoc
$user = 0 === stripos($server, 'nginx') ? 'li' : (0 === stripos($server, 'Apache/2.4.25 (Debian)') ? 'sg' : 'mo');

// echo '<pre>';
// var_dump($cnxs, 'User:', $user);
// echo '</pre>';

$pdo = new PDO(
    'mysql:host=localhost;dbname='.$cnxs[$user]['db'],
    $cnxs[$user]['u'],
    $cnxs[$user]['pw'],
    [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    ]
);