<?php

/*
 * (c) Manudon - 2019
 */

// $products = $pdo->query('SELECT * FROM products LIMIT 7')->fetchAll();
define('PER_PAGE', 4);

$query = 'SELECT * FROM products';
$params = [];
$queryCount = 'SELECT COUNT(id) as count FROM products';
// la variable sortable permet de vérifier que les champs sortables
$sortable = ['id', 'name', 'city', 'price', 'address'];


//* Rechercher par ville
if (!empty($_GET['q'])) {
    $query .= ' WHERE city LIKE :city';
    //ajouter de la variable $queryCount pour que les boutons précédente et suivante prennent en compte la recherche par lettre ou par mot
    $queryCount .= ' WHERE city LIKE :city';
    $params['city'] = '%'.$_GET['q'].'%';
}
//?Infos particulière
//@dl 
//@not À noter...
//@f 
//@Bug 

//* Organisation permet de faire le sort de manière croissante et décroissante
if(!empty($_GET['sort']) && in_array($_GET['sort'], $sortable)){
    $direction = $_GET['dir'] ?? 'asc';
    if(!in_array($direction, ['asc', 'desc'])){
        $direction = 'asc';
    }
    $query .= " ORDER BY " . $_GET['sort'] . " $direction";
}

//* Pagination
$page = (int) ($_GET['p'] ?? 1);
$offset = ($page - 1) * PER_PAGE;
$query .= ' LIMIT '.PER_PAGE." OFFSET ${offset}";

$statement = $pdo->prepare($query);
$statement->execute($params);
$products = $statement->fetchAll();

$statement = $pdo->prepare($queryCount);
$statement->execute($params);
$count = (int) $statement->fetch()['count'];
//var_dump($count);
$pages = ceil($count / PER_PAGE);

// echo '<pre>';
// print_r($products);
// echo '</pre>';

//pr($pages);
//pr([1,2,'a'=>456]);

// attention, la différence, avec svd() et spr(), c'est qu'elle ne fonc tionnent qu'avec des variables, et c'est vrai que pour le moment, g pas encore blindé cela....

//(ces nom pour les initiales de Super Var Dump et Super Print R)

$ch1 = 'lio';

$monTablo = [1, 2, 'a' => 456, 1 / 3];

$monObjet = new StdClass();