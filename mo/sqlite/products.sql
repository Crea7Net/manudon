-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  sam. 31 août 2019 à 09:50
-- Version du serveur :  10.1.36-MariaDB
-- Version de PHP :  7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `products`
--

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` double NOT NULL,
  `address` text NOT NULL,
  `cp` text NOT NULL,
  `city` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `address`, `cp`, `city`) VALUES
(1, 'Bourdon', 163893, '76, avenue Geneviève Antoine','04 197 Martinez', 'Traore-sur-Thibault'),
(2, 'Prevost SAS', 553504, 'place Amélie Navarro','93 809 Adam-la-Forêt', 'Vaillant'),
(3, 'Pascal', 407563, '56, rue Anouk Etienne','48799 Ramos', 'Baillyboeuf'),
(4, 'Laroche Bonneau S.A.S.', 328957, '7, place Michel Louis','21294 Tanguy-sur-Giraud', 'Ribeiro-sur-Pons'),
(5, 'ulien S.A.R.L.', 789402, '4, chemin de Techer','73 960 Perrier', 'Guillon'),
(6, 'Chevallier', 330611, '17, chemin Thibaut Courtois','59 205 Fontaine', 'Courtois'),
(7, 'Seguin S.A.R.L', 714005, '65, place Pinto','15 456 Dupuy', 'Marie-sur-Camus'),
(8, 'Gaudin', 425785, 'impasse Baron','70795 Roger', 'Da Costa'),
(9, 'Gerard Pires S.A.S.', 725094, '900, rue Jacob','00958 Grosdan', 'Da CostaVille'),
(10, 'Neveu', 382029, '18, boulevard Philippe Delmas','97979 Buisson', 'Hernandezdan'),
(11, 'Descamps S.A.', 429124, 'place Roger Delorme','94621 Martins', 'Muller-sur-Leclerc'),
(12, 'Nguyen SARL', 308354, '484, impasse Jean Lebreton','45 802 Dos SantosBourg', 'Joseph'),
(13, 'Philippe', 496874, '3, rue Baudry','25115 Pineau-sur-Roy', 'Lecoq'),
(14, 'Humbert SAS', 745166, 'avenue Charles Godard','79247 HamonVille', 'Joseph'),
(15, 'Gros S.A.S', 409523, '2, boulevard de Roussel','74 014 Levy', 'Samson-les-Bains'),
(16, 'Lombard Bodin S.A.R.L.', 583590, 'avenue Victoire Munoz','42 903 Auger', 'Lopes-sur-Hamel'),
(17, 'Valentin SA', 252650, '78, rue Aubert','06 241 Guillot-sur-Parent', 'Bailly'),
(18, 'Duval', 680351, 'rue de Marchal','13 988 Morelnec', 'Becker-la-Forêt'),
(19, 'Pinto', 655650, '0, avenue Étienne Moreau','25260 Marin-sur-Marechal', 'Blancnec'),
(20, 'Maurice Laine SARL', 462923, '34, chemin de Rossi','59410 Couturierdan', 'Tessiernec'),
(21, 'Descamps', 785082, '187, place de Mace','96 406 Robin', 'De Oliveira-les-Bains'),
(22, 'Daniel SARL', 377372, '1, rue Tristan Hubert','18288 Herve-sur-Daniel', 'Lemoine'),
(23, 'Ribeiro et Fils', 119435, 'avenue de Pottier','08 772 Guyon-sur-Collin', 'Simonboeuf'),
(24, 'Leblanc S.A.', 916892, '48, rue Henry','35 474 Meunier', 'Bourdon-sur-Mer'),
(25, 'Moreno S.A.', 913234, '193, rue Hamon','62397 Robert', 'Dos Santos'),
(26, 'Vallee', 759720, 'impasse de Jean','93863 Aubert-sur-Mary', 'Delahaye'),
(27, 'Briand', 399792, '9, rue de Leconte','26 791 Rey', 'Duvalnec'),
(28, 'Bouvet Lucas et Fils', 511295, '69, chemin Nath Rousset','25117 Bonneau', 'De OliveiraVille'),
(29, 'Rey Bouvier SAS', 856081, '20, boulevard de Bertin','45193 Dufour-la-Forêt', 'Jacquet'),
(30, 'Laporte', 592952, '947, rue Moreau','43 354 Cousinnec', 'Delmas-sur-Blin'),
(31, 'De Oliveira', 774156, '415, rue Berthelot','68318 Collin', 'Brun'),
(32, 'Garcia', 346285, '155, place de Guillaume','67461 Leger', 'Diaz'),
(33, 'Hardy Roy SARL', 968917, '56, rue Martinez','94250 Tessier', 'Lucas'),
(34, 'Giraud', 961752, '1, chemin Lombard','73542 PiresBourg', 'Jolynec'),
(35, 'Berger Meunier S.A.R.L.', 518779, '18, boulevard Pottier','46911 Barbierboeuf', 'Pruvost'),
(36, 'Munoz et Fils', 179138, '54, boulevard de Lopes','53303 Delahaye', 'Aubert-la-Forêt'),
(37, 'Albert', 833213, 'boulevard Anaïs Fischer','22648 DupuisVille', 'Guillaume'),
(38, 'Lambert', 551996, '97, boulevard Anaïs Dias','40643 Moreau', 'Benard-sur-Adam'),
(39, 'Bailly SAS', 837375, '62, place Chartier','32886 GimenezBourg', 'Francois'),
(40, 'Dubois', 894866, '91, chemin Jacob','60 044 Ferreira-les-Bains', 'Techer-sur-Mer'),
(41, 'Jacob', 308962, '80, rue de Brunet','76760 Guillotboeuf', 'Mercier'),
(42, 'Martineau', 171986, '22, rue de Pineau','78 929 LemaitreVille', 'Mahe'),
(43, 'Cordier S.A.S.', 854753, 'rue de Bousquet','78090 Lenoir', 'Barbierboeuf'),
(44, 'Delannoy', 751148, '8, avenue Susan Marty','34 768 Maury-sur-Mer', 'MenardVille'),
(45, 'Marin', 836769, '33, rue Alves','35 036 LamyBourg', 'Ferreira-sur-Simon'),
(46, 'Riou Wagner S.A.R.L.', 184544, '79, rue Roger Verdier','47 067 Baudrydan', 'FaureBourg'),
(47, 'Besnard', 510510, '34, boulevard Mathilde Dupuis','71 017 Richard', 'MilletVille'),
(48, 'Riou Barbier SAS', 401959, '9, rue Dijoux','23588 Perret-la-Forêt', 'Hebert-sur-Remy'),
(49, 'Martins Pineau SARL', 987872, '40, rue de Goncalves','03075 Leroux', 'Carlier'),
(50, 'Gilbert Renard et Fils', 165794, '92, rue Rolland','78 121 David', 'Perrot'),
(51, 'Lopez S.A.S.', 266576, '8, impasse Victoire Blanchet','45 232 Barthelemy-sur-Descamps', 'Herve'),
(52, 'Camus', 870797, '355, place Valentine Rousset','36 446 Gomez-les-Bains', 'Paul-sur-Mer'),
(53, 'Paris', 251680, '4, impasse Richard','31 788 Leleu-sur-Arnaud', 'Hoarau'),
(54, 'Camus S.A.', 614514, '4, impasse Richard','31 788 Leleu-sur-Arnaud', 'Hoarau'),
(55, 'Camus S.A.', 614514, '47, boulevard de Boucher','29725 ColinVille', 'De Sousa'),
(56, 'Barbe', 241532, '423, boulevard de Petitjean','32 783 Louisboeuf', 'Juliendan'),
(57, 'Philippe', 802165, 'ue Patrick Delahaye','99 260 Laroche', 'Delaunay'),
(58, 'Chauveau', 111019, '2, impasse Schmitt','46071 Faure-les-Bains', 'PerrierVille'),
(59, 'Loiseau Louis SA', 950346, '36, place Augustin Bousquet','48531 Renaultdan', 'Boyer-la-Forêt'),
(60, 'Francois S.A.R.L.', 595876, '96, avenue Vincent Chauvin','42142 Auger', 'Meyer'),
(61, 'Vaillant Charles SAS', 368438, 'impasse de Bouvet','15 413 Joly', 'Perez'),
(62, 'Legendre', 669799, '5, boulevard Wagner','20217 Ramos', 'Gaudin'),
(63, 'Da Costa', 330019, '20, rue de Rolland','99 820 Perret', 'Lejeunenec'),
(64, 'Chartier', 248603, 'rue de Besnard','54312 Gautierboeuf', 'Martinez'),
(65, 'Lopez Michaud S.A.', 723055, '3, chemin de Thierry','72 083 Hebert', 'MarchandVille'),
(66, 'Maillet', 579403, '7, rue Étienne Lefebvre','73 219 Etienne', 'SchmittBourg'),
(67, 'Dias Riou S.A.', 540190, '63, avenue Traore','43274 Chevalier', 'GauthierVille'),
(68, 'Voisin Huet S.A.S.', 271355, 'rue Suzanne Vaillant','08697 Gonzalezboeuf', 'Gautierdan'),
(69, 'Vaillant Pineau S.A.S.', 981884, 'rue Susanne Le Gall','51 998 Delorme-les-Bains', 'Lefebvre'),
(70, 'Brunel Masse S.A.R.L.', 189002, '814, impasse Théophile Valentin','42 223 Bazin', 'Legrand-sur-Martinez'),
(71, 'Rocher', 835189, '43, impasse de Fouquet','64723 FleuryVille', 'Gillet-sur-Guyon'),
(72, 'Jean', 212199, '59, boulevard Collin','52 792 Robert-sur-Mer', 'Baron'),
(73, 'Moreau S.A.S.', 532760, '57, boulevard Carpentier','83 192 Moreno', 'Torres'),
(74, 'Masson S.A.', 983295, '69, impasse Thierry Fleury','13 477 Meunier', 'Gilbert'),
(75, 'Hardy Le Goff SARL', 554250, '28, boulevard de Lemonnier','95478 Leclercq', 'Bousquet-sur-Mer'),
(76, 'Maury', 425303, '5, avenue Arnaud','89393 Bouchetnec', 'Lemonnier'),
(77, 'Adam SARL', 850168, 'rue Théophile Bruneau','49847 Bourgeois', 'Marion-sur-Mer'),
(78, 'Gay', 281242, '82, avenue de Perez','05 927 Payet', 'Berger'),
(79, 'Voisin Boutin S.A.', 941556, 'boulevard de Guillot','64476 Merlenec', 'Delorme'),
(80, 'Mathieu Collet SAS', 176054, '31, chemin Pages','54 497 MorelVille', 'Toussaintboeuf'),
(81, 'Breton', 399399, '402, place Zacharie Samson','82 821 Alexandrenec', 'Bousquet-sur-Foucher'),
(82, 'Blondel', 127995, '7, impasse Marcelle Michaud','93 815 Mallet', 'BourdonBourg'),
(83, 'Delannoy', 626977, '76, rue Alexandre','68 219 Evrard', 'Nguyen'),
(84, 'Chevalier', 250235, '2, place Denis','55 878 Lambert', 'Normand'),
(85, 'Hardy Munoz S.A.S.', 731050, 'boulevard Gomes','48060 Grenier', 'Albert-sur-Bouvier'),
(86, 'Thibault SA', 285668, '814, impasse de De Sousa','84 732 Rossi', 'Jourdan-les-Bains'),
(87, 'Guilbert', 905712, '896, impasse de Bazin','88 452 Thibault-les-Bains', 'Wagner'),
(88, 'Chauvin Gomez S.A.', 301541, '35, chemin de Thierry','22969 Giraud', 'Rodrigues-sur-Mer'),
(89, 'Picard S.A.', 826338, 'avenue Camille Garcia','02 127 Meunier', 'Guyotnec'),
(90, 'Morvan', 101232, '27, place de Martinez','90 565 Cousin', 'Bourgeois-sur-Joly'),
(91, 'aure et Fils', 195249, '8, chemin de Leleu','62116 Lombard', 'Guyon'),
(92, 'Bernard', 374492, 'rue Eugène Traore','65770 Poulain', 'Bouvier'),
(93, 'Martel', 193257, '580, impasse de Lombard','85 002 Henry-la-Forêt', 'Duhamel'),
(94, 'Delannoy Morvan et Fils', 613845, '27, rue de Le Goff','37 432 Pasquier', 'Hardyboeuf'),
(95, 'Didier Loiseau S.A.', 201986, '4, impasse de Joubert','91883 Camus', 'Martin'),
(96, 'Perrin Levy S.A.S.', 379654, '183, avenue de Guilbert','10542 Lefort', 'Blanchard-sur-Delmas'),
(97, 'Marechal', 546104, '8, avenue Lucie Mercier','78 165 Fischer', 'Rochernec'),
(98, 'Delmas Klein S.A.', 363852, 'rue de Dubois','83246 DelormeVille', 'Roche'),
(99, 'Bousquet SARL', 177738, '40, boulevard Susanne Henry','61 079 Gerard', 'Bertin'),
(100, 'Buisson Gonzalez et Fils', 503822, '81, rue de Michel','98074 Laroche-les-Bains', 'Julien');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
