<div class="row align-content-around flex-wrap m-auto">

    <?php

// echo dirname(__DIR__).'/exemples<br>'.'..'.__DIR__;
$dossExemples = 'exemples/'; // Dossier des exemples
$files        = new DirectoryIterator(dirname(__DIR__).'/'.$dossExemples);
// var_dump($files, pathinfo(__FILE__));

// echo '<pre class="text-left">';
// print_r($files);
// echo '</pre>';

// 2Do: Trouver le moyen de trier lesfichiers
$fichiers = [];
foreach ($files as $fileInfo) {
    // var_dump($fileInfo);
    if ($fileInfo->isDot() || $fileInfo->isDir()) {
        continue;
    }
    $ext = $fileInfo->getExtension();
    // echo $fileInfo->getBasename($ext).'<br>';
    // echo 'Courant: '.$fileInfo->key().'<br>';
    $fichiers[$fileInfo->key()]['name'] = $fileInfo->getBasename($ext);
    $fichiers[$fileInfo->key()]['ext']  = $ext;

    // echo $ext;
}
rsort($fichiers);
// echo '<hr><pre class="text-left">';
echo '<div class="row col-12">'.count($fichiers).' exemples:</div>';
// echo '</pre>';

    foreach ($fichiers as $fichier) {
        // echo '<hr><pre class="text-left">';
        // echo $fichier['name'];
        // echo '</pre>'; ?>
    <div class="col shadow px-4 py-2 m-2 bg-white rounded <?= ('php' === $fichier['ext']) ? 'bgcmaroon' : ''; ?>"><a
            href=<?php echo $dossExemples.$fichier['name'].$fichier['ext']; ?> target="_blank"
            class="text-decoration-none font-weight-bold">
            <?php echo ucfirst(substr($fichier['name'], 0, -1)); ?>
        </a>
    </div>
    <?php
    }

    // var_dump($a);
    ?>
</div>