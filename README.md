# Manudon

Base de dev collaboratif
www.ManuDon.com


---

## Description

Projet de dev pour **J**ean-**L**uc, **MO**mo, **ROM**ain

& **GC7** *(Lionel)*

**Tout nouveau membre ne peut rejoindre le club, qu'invité par un membre actif actuel**

## [Une question ?](https://gitlab.com/grcote7/manudon/issues)

### Buts

- [x] 1. Avoir un outil de dev identique et apprendre tout avec l'aide d'un autre membre.
- [x] 2. Mise à niveau des compétences  dans les langages de base 'HTML, CSS, JS, PHP, etc...)
- [x] 3. ...

## [Une question ?](https://gitlab.com/grcote7/manudon/issues)
---

Chacun y a son dossier pour y faire ce qu'il veut :-) !

[@ venir]

(Chacun y appose sa propre croix)

La page d'index montre où on en est

----
*Rappel*

**Dès le début, à chaque étape, pour toute difficulté, soluce avec ton tuteur**

### Moyens

#### Pré-recquis:
1. Avoir un compte GitLab.com (GL) et y être logué
2. Installer GitKraken.com (GK) et [y authentifier son compte GL](https://c57.fr/doc/outils/gitkraken)
3. Monter [VAGRANT](ANNEXE.md)
4. Fork le projet [MANUDON](https://GitLab.com/GrCOTE7/manudon)
   (*Créer une divergence*)
5. Clone en local de ce fork dans le dossier localhost
6. Réaliser VHOST
7. DEV

## [Une question ?](https://gitlab.com/grcote7/manudon/issues)
