<?php
require_once __DIR__.'/demodata.php';
// require_once $path.'/gc7/tutos/config.php';
/*
vd(CRENEAUX);
Tuto Les dates - https://www.grafikart.fr/tutoriels/dates-php-1124
*/
?>
<div class="section">

    <div class="row">

        <div class="col-sm-7">
            <h2>Nous contacter</h2>
            <p class="text-justify">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vitae excepturi dicta
                asperiores, voluptates
                aliquid ratione blanditiis vel tenetur quod quibusdam nesciunt eaque eius voluptate itaque non nemo
                voluptatum nisi dignissimos.</p>
        </div>

        <div class="col-sm-5 mt-2">
            <h5 class="bg-dark text-light text-center rounded w-100 p-1">Horaires d'ouvertures</h5>
            <ul class="list-group-item list-unstyled rounded">

                <?php foreach (JOURS as $k => $jour) { ?>

                <li class="list-decoration-none">
                    <?php echo '<strong>'.$jour.'</strong>: '.creneaux_html(CRENEAUX[$k]); ?>
                </li>

                <?php } ?>

            </ul>
        </div>

    </div>
</div>