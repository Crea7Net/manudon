<?php

/*
 * (c) Manudon - 2019
 */

$listing = function (...$items): array {
    $arr = [];
    foreach ($items  as $item) {
        $arr[] = $item;
    }

    return $arr;
};

vd($listing, $listing(1, [2, 3]));