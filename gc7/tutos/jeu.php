<?php

$aDeviner = 150;
$value    = null;
vd($_POST);
if (isset($_POST['chiffre'])) {
    $value = htmlentities($_POST['chiffre']); // Nécessaire que pour GET
    if ($value > $aDeviner) {
        echo 'Votre chiffre est trop grand';
    } elseif ($value < $aDeviner) {
        echo 'Votre chiffre est trop petit';
    } else {
        echo 'Gagné !';
    }
}
// echo $_SERVER['PHP_SELF'];
?>

<form action="p.php?page=tutos/jeu" method="POST">
    <div class="form-group">
        <input type="number" class="form-control" name="chiffre" placeholder="Entre 0 et 1000"
            value="<?php echo $value; ?>">

        <button type="submit" class="btn btn-primary btn-sm my-3">
            Deviner
        </button>

    </div>
</form>