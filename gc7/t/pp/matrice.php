<?php
ini_set('display_errors', '1');
$path    = 'manudon.com' === $_SERVER['HTTP_HOST'] ? '/gc7' : '';
$base    = getenv('SERVER_NAME');
$serveur = ('manudon.com' === $base) ? $base : 'manudon';
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="../../favicon.ico" />
    <title><?php echo $titre ?? 'Matrice'; ?> |
        GC7</title>

    <link rel="stylesheet" href="../../assets/css/frontend.css" />
    <link rel="stylesheet" href="<?php echo $path; ?>/assets/css/style.css" />
    <style>
    #manudon {
        position: fixed;
        z-index: 9;
        top: 0;
        right: 10px;
    }
    </style>
</head>

<body>

    <div id="manudon">
        <a style="z-index:9" href="http://<?php echo $serveur; ?>" title="Accueil ManuDon"><i
                class="fa fa-home"></i></a>
    </div>
    <?php
    // include_once '../../../config/home.php';
    include_once '../../tools/vd.php';

    // $a = [1, 2];
    // svd('a');

    ?>

    <!-- <p class="text-center">Bootstrap OK</p> -->
    <div class="container-fluid">
        <div class="main text-center">
            <h1>MATRICE</h1>
        </div>
    </div>

    <?php include '../../assets/footer_txt.html'; ?>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://use.fontawesome.com/0b5fa10a1b.js"></script>

    <script src="<?php echo $path; ?>/assets/js/app.js"> </script>

</body>

</html>
<?php
/* <script src="<?=__DIR__?>/js/app.js"> </script>" */