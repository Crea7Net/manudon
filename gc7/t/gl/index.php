<?php
ini_set('display_errors', '1');
$path    = 'manudon.com' === $_SERVER['HTTP_HOST'] ? '/gc7' : '';
$base    = getenv('SERVER_NAME');
$serveur = ('manudon.com' === $base) ? $base : 'manudon';
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="../../favicon.ico" />
    <title><?php echo $titre ?? 'GL'; ?> |
        GC7</title>

    <link rel="stylesheet" href="<?php echo $path; ?>/assets/css/frontend.css" />
    <link rel="stylesheet" href="<?php echo $path; ?>/assets/css/style.css" />

    <link rel="stylesheet" href="<?php echo $path; ?>/t/gl/gl.css" />
</head>

<body>
    <script src="vue_1_0_24.js"></script>
    <div id="manudon"><a style="z-index:9" href="http://<?php echo $serveur; ?>" title="Accueil ManuDon"><i
                class="fa fa-home"></i></a></div><?php // include_once '../../../config/home.php';
    include_once '../../tools/vd.php';

    // $a = [1, 2];
    // svd('a');

    ?>
    <!-- <p class="text-center">Bootstrap OK</p>-->

    <div class="container-fluid">
        <div class="main text-center" id="demo">
            <div class="row">
                <h1>Commits (<a target="_blank" title="Ouvrir GitLab" href="https://gitlab.com/c57fr/manudon"
                        target="_blank">GitLab</a>)
                </h1>
            </div>
        </div>


        <label v-for="branch in branches" class="spaceli">

            <input type="radio" name="branch" :id="branch" :value="branch" v-model="currentBranch">

            <label :for="branch">
                {{branch}}
            </label>

        </label>

        <p>vuejs/vue@{{currentBranch}}</p>


        <ul>
            <li v-for="record in commits" class="spaced">
                <a :href="record.html_url" target="_blank" class="commit">{{record.sha.slice(0, 7)}}</a>
                - <span class="message">{{record.commit.message|truncate}}</span><br>
                by <span class="author">{{record.commit.author.name}}</span>
                at <span class="date">{{record.commit.author.date|formatDate}}</span>
            </li>
        </ul>

        <div class="log">Réponse</div>

    </div>


    <?php include '../../assets/footer_txt.html';
    ?><script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://use.fontawesome.com/0b5fa10a1b.js"></script>

    <script src="gl.js"></script>

</body>

</html><?php
    /* <script src="<?=__DIR__?>/js/app.js"> </script>" */