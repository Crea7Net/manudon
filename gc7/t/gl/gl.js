$(document).ready(function() {

    /**
     * Created by cote on 15/05/2016.
     */
    var apiURL = 'https://api.github.com/repos/GrCOTE7/OctoberCMS000/commits?per_page=7&sha='
    //var apiURL = 'https://api.github.com/repos/vuejs/vue/commits?per_page=3&sha='
    var isPhantom = navigator.userAgent.indexOf('PhantomJS') > -1

    /**
     * Test mocks
     */

    var mocks = {
        master: [{
            sha: '111111111111',
            commit: {
                message: 'one',
                author: {
                    name: 'Evan',
                    date: '2014-10-15T13:52:58Z'
                }
            }
        }, {
            sha: '111111111111',
            commit: {
                message: 'hi1',
                author: {
                    name: 'Evan',
                    date: '2014-10-15T13:52:58Z'
                }
            }
        }, {
            sha: '111111111111',
            commit: {
                message: 'hi1-2',
                author: {
                    name: 'Evan',
                    date: '2014-10-15T13:52:58Z'
                }
            }
        }],
        dev: [{
            sha: '222222222222',
            commit: {
                message: 'two',
                author: {
                    name: 'Evan',
                    date: '2014-10-15T13:52:58Z'
                }
            }
        }, {
            sha: '111111111111',
            commit: {
                message: 'hi2-1',
                author: {
                    name: 'Evan',
                    date: '2014-10-15T13:52:58Z'
                }
            }
        }, {
            sha: '111111111111',
            commit: {
                message: 'hi2-2',
                author: {
                    name: 'Evan',
                    date: '2014-10-15T13:52:58Z'
                }
            }
        }]
    }

    function mockData() {
        this.commits = mocks[this.currentBranch]
    }

    /**
     * Actual demo
     */

    var demo = new Vue({

        el: '#demo',

        data: {
            branches: ['master', 'gc000'],
            currentBranch: 'gc000',
            commits: null
        },

        created: function() {
            this.fetchData()
        },

        watch: {
            currentBranch: 'fetchData'
        },

        filters: {
            truncate: function(v) {
                var newline = v.indexOf('\n')
                return newline > 0 ? v.slice(0, newline) : v
            },
            formatDate: function(v) {
                return v.replace(/T|Z/g, ' ')
            }
        },

        methods: {
            fetchData: function() {
                // CasperJS fails at cross-domain XHR even with
                // --web-security=no, have to mock data here.
                if (isPhantom) {
                    return mockData.call(this)
                }
                var xhr = new XMLHttpRequest()
                var self = this
                xhr.open('GET', apiURL + self.currentBranch)
                xhr.onload = function() {
                    self.commits = JSON.parse(xhr.responseText)
                    console.log(self.commits[0].html_url)
                    console.log(self.commits[0])
                    // console.log(self.commits[0].author.date)
                    // console.log(self.commits[0].author.name)
                }
                // console.log(self.commits[0]);
                xhr.send()
            }
        }
    })


    $(document).ajaxSuccess(function(event, xhr, settings) { // 
        console.log('Fini:' + xhr.commits);
        $(".log").html("Triggered ajaxSuccess handler. The Ajax response was:<br><br>" +
            xhr.demo);
    });

    // ... 2do tester
    // xhr.onreadystatechange = () => {
    //      if (this.readyState === XMLHttpRequest.DONE) {
    // ...


})
