<?php
//? Bon path depuis la racine
$path = 'manudon.com' === $_SERVER['HTTP_HOST'] ? '/gc7' : '';

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="../favicon.ico" />
    <title><?php echo $titre ?? 'Accueil'; ?> | GC7</title>

    <link rel="stylesheet" href="<?=$path?>/assets/css/frontend.css" />
    <link rel="stylesheet" href="<?php echo $path; ?>/assets/css/style.css" />
</head>

<body>
    <?php
    include_once __DIR__.'/../../config/home.php';
    include_once __DIR__.'/../tools/vd.php';
    // $opSG  = $path.'ok';
    // $opGC7 = $path.'ok7';

    ?>
    <!-- <p class="text-center">Bootstrap OK</p> -->
    <div class="container-fluid">
        <div class="main">