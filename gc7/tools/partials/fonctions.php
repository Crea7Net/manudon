<?php

/*
 * (c) Manudon - 2019
 */

/*
* Retourne le code HTML pour l'affichage dans une liste groupée selon bootstrap.
*
* On appelle ce type de fonction un helper
*
* @param string $sujet
* @param string $lien
* @param string [$ext] true pour nouvelle fenêtre
*/
$lk = function (string $titre, string $lien, bool $ext = null): string {
    return '<li class="list-group-item p-1 px-3"><a href="a/p.php?page='.$lien.'" target="'.($ext).'" class="card-link">'.$titre.'</a></li>';
};

/*
* Retourne camelCase ou snake_caseen Humain case 
*/
$humanize = function (string $text): string {
    return ucfirst(trim(mb_strtolower(preg_replace(['/([A-Z])/', '/[_\s]+/'], ['_$1', ' '], $text))));
};

$cards_menu = function ($pages) use ($humanize, $lk) {
    $cols = 'col-xl-2 col-lg-3 col-md-4 col-sm-6 my-2';

    $html = '<div style="z-index:0"><div class="row justify-content-center" style="z-index:0">';

    foreach ($pages as $k => $page) {
        $html .= '<div class="'.$cols.'">
    <div class="card">
        <div class="card-header text-center letspace">'
            .strtoupper($k).
            '</div>
        <ul class="list-group list-group-flush">';
        foreach ($page as $lien) {
            $titre = $humanize(titre($lien));
            $html .= $lk($titre, $lien);
            // svd('titre', 'lien');
        }
        $html .= '</ul>
    </div>
</div>';
    }
    $html .= '</div>';

    return $html;
};